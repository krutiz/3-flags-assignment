'USE STRICT';

const $ = function (nml) { return document.getElementById(nml); };

const flag = function () {
    let canvas = $('myCanvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        ctx.beginPath();
        ctx.arc(130, 120, 50, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 8;
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(250, 120, 50, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 8;
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(370, 120, 50, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'red';
        ctx.lineWidth = 8;
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(190, 170, 50, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'yellow';
        ctx.lineWidth = 8;
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(310, 170, 50, 0, Math.PI * 2, true);
        ctx.strokeStyle = 'green';
        ctx.lineWidth = 8;
        ctx.stroke();

    }
}

window.addEventListener('load', flag);
