'USE STRICT';

const $ = function (nml) { return document.getElementById(nml); };

const flag = function () {
    let canvas = $('myCanvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        ctx.fillStyle = "red";
        ctx.fillRect(0, 0, 130, 130);

        ctx.fillStyle = "red";
        ctx.fillRect(0, 170, 130, 130);

        ctx.beginPath();
                ctx.moveTo(170, 0);
                ctx.lineTo(600, 0);
                ctx.lineTo(450, 130);
                ctx.lineTo(170, 130);
                ctx.fillStyle = 'red';
                ctx.fill();

        ctx.beginPath();
                ctx.moveTo(170, 300);
                ctx.lineTo(170, 170);
                ctx.lineTo(450, 170);
                ctx.lineTo(600, 300);
                ctx.fillStyle = 'red';
                ctx.fill();

    }
}

window.addEventListener('load', flag);
