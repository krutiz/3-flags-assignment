'USE STRICT';

const $ = function (nml) { return document.getElementById(nml); };

const flag = function () {
    let canvas = $('myCanvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');

        ctx.fillStyle = "red";        
        ctx.fillRect(0, 150, 500, 150);

        ctx.beginPath();
        ctx.arc(150, 150, 90, 0, Math.PI, true);
        ctx.fillStyle = 'red';
        ctx.fill();

        ctx.beginPath();
        ctx.arc(150, 150, 90, 0, Math.PI, false);
        ctx.fillStyle = 'white';
        ctx.fill();

    }
}

window.addEventListener('load', flag);
